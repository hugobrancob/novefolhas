const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const uuid = require('uuid');
const mysql = require('mysql');

app.use(cors({origin: true, credentials: true}));

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

/* GERAR CV - SUBMIT */
app.post('/app/gerar-cv', function(req, res) {

  const id = uuid.v1();
  let pool;

  const createPool = async () => {
    pool = await mysql.createPool({
      user: 'root', 
      password: 'n0v3db5479', 
      database: 'nove_dados', 
      host: '127.0.0.1',
      port: 3306, 

      /*
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      */

    });
  };

  jsonclean = (str) => { return str.replace(/\\/g, "\\\\").replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t").replace(/\f/g, "\\f").replace(/"/g,"\\\"").replace(/'/g,"\\\'").replace(/\&/g, "\\&")}

  /* tratar dados */
  let dados = req.body.dados;
  const cv = jsonclean(JSON.stringify(dados));

  const tema = dados.tema;
  const newsletter = dados.newsletter;
  const termos = dados.termos;
  const nome = jsonclean(dados.cabecalho.nome); 
  const email = jsonclean(dados.cabecalho.email[0].value); 
  const telefone = jsonclean(dados.cabecalho.telefone[0].value);
  delete dados['tema'];

  createPool().then(async function () {

    if(newsletter === true) {
      const origem = 'gerar-cv';
      if (dados.cabecalho.email[0].value.length > 0) {
        try {
          const newsQuery = await pool.query(`INSERT INTO newsletter(id, nome, email, time, origem) VALUES ('` + id + `', '` + nome + `', '` + email + `', NOW(), '` + origem + `');`);
        } catch (err) {
          console.log(err);
        }
      }

      if(termos === true) {
        try{
          const curriculoQuery = await pool.query(`INSERT INTO curriculos(id, nome, email, telefone, tema, cv, time) VALUES ('`+id+`', '` + nome + `', '` + email + `', '` + telefone + `', '` + tema + `', '` + cv + `', NOW())`, function(err, results, fields) {
            if(!err) {
              res.send({curriculo: true});
            }
          });
        } catch(err) {
          console.log(err);
        }
      }
    }

  });

});

app.listen(8080, () => {
  console.log('');
  //console.log('App listening on port 8080');
  //console.log('Press Ctrl+C to quit.');
});